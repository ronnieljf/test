import { IsNotEmpty, IsString, IsEmail } from 'class-validator';

export class RegisterDTO {
  @IsString()
  @IsNotEmpty()
  @IsEmail()
  email: string;
  @IsString()
  @IsNotEmpty()
  password: string;
}
