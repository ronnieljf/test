/* eslint-disable @typescript-eslint/no-this-alias */
/* eslint-disable @typescript-eslint/ban-types */
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type HitDocument = Hit & Document;

@Schema()
export class Hit {
  @Prop({ required: true })
  created_at: Date;

  @Prop()
  title: string;

  @Prop()
  url: string;

  @Prop({ required: true })
  author: string;

  @Prop()
  points: string;

  @Prop()
  story_text: string;

  @Prop()
  comment_text: string;

  @Prop()
  num_comments: number;

  @Prop()
  story_id: number;

  @Prop()
  story_title: string;

  @Prop()
  story_url: string;

  @Prop()
  parent_id: number;

  @Prop()
  created_at_i: number;

  @Prop([String])
  _tags: string[];

  @Prop({ unique: true })
  objectID: string;

  @Prop([Object])
  _highlightResult: any[];

  @Prop({ default: false, required: true, select: false })
  erased: boolean;
}

export const HitSchema = SchemaFactory.createForClass(Hit);
