import { Module } from '@nestjs/common';
import { HitService } from './hit.service';
import { HitController } from './hit.controller';
import { HttpModule } from '@nestjs/axios';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';
import { Hit, HitSchema } from '../schemas/hit.schema';
import { CommandModule } from 'nestjs-command';
import { HitsSeed } from './hits.seed';
@Module({
  imports: [
    HttpModule,
    ScheduleModule.forRoot(),
    MongooseModule.forFeature([{ name: Hit.name, schema: HitSchema }]),
    CommandModule,
  ],
  providers: [HitService, HitsSeed],
  controllers: [HitController],
})
export class HitModule {}
