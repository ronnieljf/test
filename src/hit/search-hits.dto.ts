import { IsInt, IsString, IsOptional } from 'class-validator';
export class SearchHitsDto {
  @IsString()
  @IsOptional()
  author: string;
  @IsString()
  @IsOptional()
  story_title: string;
  @IsString()
  @IsOptional()
  month: string;
  @IsString()
  @IsOptional()
  tags: string;
  @IsInt()
  @IsOptional()
  page: number;
}
