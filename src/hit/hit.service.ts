import { Injectable, Logger } from '@nestjs/common';
import { Model } from 'mongoose';
import { Cron } from '@nestjs/schedule';
import { HttpService } from '@nestjs/axios';
import { InjectModel } from '@nestjs/mongoose';
import { Hit, HitDocument } from '../schemas/hit.schema';
import { CONSTANTS } from '../utils/constanst';
import { SearchHitsDto } from './search-hits.dto';

@Injectable()
export class HitService {
  private readonly logger = new Logger(HitService.name);
  constructor(
    private httpService: HttpService,
    @InjectModel(Hit.name) private hitModel: Model<HitDocument>,
  ) {}

  async deleteHit(objectID: string): Promise<any> {
    const hit = await this.hitModel.updateMany({ objectID }, { erased: true });
    return hit;
  }

  async getHits(searchHitsDto: SearchHitsDto): Promise<Hit[]> {
    const $expr = { $and: [] };
    const $and = [];
    const query = {
      $and: [],
      $expr: {},
      erased: false,
    };

    const pagination = 5;

    if (searchHitsDto.author)
      $and.push({
        author: { $regex: searchHitsDto.author },
      });

    if (searchHitsDto.story_title)
      $and.push({
        story_title: { $regex: searchHitsDto.story_title },
      });

    if (searchHitsDto.tags)
      $and.push({
        _tags: { $in: [searchHitsDto.tags] },
      });

    if (searchHitsDto.month && searchHitsDto.month.length > 2) {
      const month = CONSTANTS.MONTHS.find((m) =>
        m.name.startsWith(searchHitsDto.month.toUpperCase()),
      );
      if (month)
        $expr.$and.push({ $eq: [{ $month: '$created_at' }, month.number] });
    }

    if ($and.length > 0) query.$and = $and;
    else delete query.$and;
    if ($expr.$and.length > 0) query.$expr = $expr;
    else delete query.$expr;

    return this.hitModel
      .find(query)
      .sort({ created_at: -1 })
      .skip(
        searchHitsDto.page && searchHitsDto.page > 0
          ? (searchHitsDto.page - 1) * pagination
          : 0,
      )
      .select('-_id -__v')
      .limit(pagination);
  }

  @Cron('0 0 * * * *')
  private async start() {
    const resp = await this.httpService
      .get(process.env.HACKER_BASE_URL + 'search_by_date?query=nodejs')
      .toPromise();
    const data = await resp.data;
    this.hitModel.insertMany(data.hits).catch((e) => {
      if (e.message.includes('E11000 duplicate key error collection')) {
        this.insertOrUpdate(data.hits);
      }
    });
  }

  private async insertOrUpdate(data: [any]) {
    const objectIDs = [];
    const newHits = [];
    const updateHits = [];
    data.forEach((element) => {
      objectIDs.push(element.objectID);
    });
    const hits = await this.hitModel
      .find({
        objectID: { $in: objectIDs },
      })
      .select('-_id objectID');
    for (let index = 0; index < objectIDs.length; index++) {
      const objectID = objectIDs[index];
      const hit = hits.find((h) => h.objectID == objectID);
      if (hit) {
        updateHits.push(data[index]);
      } else {
        newHits.push(data[index]);
      }
    }
    if (newHits.length > 0) await this.hitModel.insertMany(newHits);
    if (updateHits.length > 0)
      updateHits.forEach(async (hit) => {
        await this.hitModel.updateOne({ objectID: hit.objectID }, hit);
      });
  }
}
