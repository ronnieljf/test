import {
  Body,
  Controller,
  Delete,
  HttpCode,
  HttpException,
  HttpStatus,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { SearchHitsDto } from './search-hits.dto';
import { Hit } from '../schemas/hit.schema';
import { HitService } from './hit.service';
import { AuthGuard } from '@nestjs/passport';

@Controller('hits')
export class HitController {
  constructor(private readonly hitService: HitService) {}

  @Post()
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'))
  async getHits(@Body() searchHitsDto: SearchHitsDto): Promise<Hit[]> {
    return this.hitService.getHits(searchHitsDto);
  }

  @Delete()
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'))
  async deleteHit(@Query('objectID') objectID): Promise<any> {
    if (!objectID)
      throw new HttpException(
        {
          statusCode: HttpStatus.BAD_REQUEST,
          message: ['objectID is required in the url query'],
          error: 'bad Request',
        },
        HttpStatus.BAD_REQUEST,
      );
    return this.hitService.deleteHit(objectID);
  }
}
